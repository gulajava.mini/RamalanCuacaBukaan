package gulajava.ramalancuaca.internets.sinkronisasi;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Gulajava Ministudio.
 */
public class RCuacaSyncServis extends Service {

    private static final Object sSyncAdapterLock = new Object();
    private static RCuacaSyncAdapter sRCuacaSyncAdapter = null;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("CUACA SINKRONISASI", "onCreate - SERVIS CUACA SINKRONISASI");

        synchronized (sSyncAdapterLock) {
            if (sRCuacaSyncAdapter == null) {
                sRCuacaSyncAdapter = new RCuacaSyncAdapter(RCuacaSyncServis.this.getApplicationContext(), true);
            }
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return sRCuacaSyncAdapter.getSyncAdapterBinder();
    }
}
