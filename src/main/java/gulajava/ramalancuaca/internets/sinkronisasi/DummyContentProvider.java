package gulajava.ramalancuaca.internets.sinkronisasi;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by Gulajava Ministudio.
 * CONTENT PROVIDER PURA PURA KARENA INI PROVIDER SEBENARNYA UNTUK
 * SQLITE YANG SUDAH GA JAMAN LAGI....
 */
public class DummyContentProvider extends ContentProvider {

    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
        return true;
    }

    /*
     * query() always returns no results
     *
     */
    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        return null;
    }

    /*
    * Return no type for MIME type
    */
    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    /*
    * insert() always returns null (no URI)
    */
    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    /*
     * update() always returns "no rows affected" (0)
     */
    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
