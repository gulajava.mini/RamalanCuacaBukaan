package gulajava.ramalancuaca.internets.modelnet.cuacaperjam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio.
 */
public class CuacaPerJams {

    private CityModel city;

    private List<ForecastJamModel> list = new ArrayList<>();

    public CuacaPerJams() {
    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public List<ForecastJamModel> getList() {
        return list;
    }

    public void setList(List<ForecastJamModel> list) {
        this.list = list;
    }
}
