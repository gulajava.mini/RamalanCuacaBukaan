package gulajava.ramalancuaca.internets.modelnet.cuacaperjam;

/**
 * Created by Gulajava Ministudio.
 */
public class WindModel {

    private double speed;
    private double deg;

    public WindModel() {
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }
}
