package gulajava.ramalancuaca.aktivitas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.database.RMSetelan;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Gulajava Ministudio.
 */
public class AktSetelan extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    private ActionBar mActionBar;
    @Bind(R.id.teks_keterangan_notifikasi)
    TextView mTextViewKeteranganTeksNotifikasi;
    @Bind(R.id.switch_notifikasi)
    SwitchCompat mSwitchCompatNotifikasi;

    private Realm mRealm;
    private RealmResults<RMSetelan> mRealmResultsSetelan;
    private RealmQuery<RMSetelan> mRealmQuerySetelan;
    private RealmChangeListener mRealmChangeListener;
    private RMSetelan mRMSetelan;
    private boolean sudahInisialisasi = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.akt_setelan);
        ButterKnife.bind(AktSetelan.this);

        AktSetelan.this.setSupportActionBar(mToolbar);
        mActionBar = AktSetelan.this.getSupportActionBar();

        assert mActionBar != null;
        mActionBar.setTitle(R.string.setelan_judul);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(AktSetelan.this);
        mRealm = Realm.getInstance(realmConfiguration);

        inisialisasiRealm();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mRealmChangeListener != null && mRealmResultsSetelan != null) {
            mRealmResultsSetelan.removeChangeListeners();
        }

        mRealm.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                AktSetelan.this.finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, final boolean isCentang) {

            switch (compoundButton.getId()) {

                case R.id.switch_notifikasi:

                    setTeksKeterangan(isCentang);

                    //perbarui database realm
                    mRealm.executeTransactionAsync(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            mRMSetelan.setNotifikasiCuaca(isCentang);
                            realm.copyToRealmOrUpdate(mRMSetelan);
                        }
                    });

                    break;
            }
        }
    };


    //INISIALISASI REALM
    private void inisialisasiRealm() {

        //listener realm jika ada perubahan
        //salin realm objek untuk keperluan penyimpanan setelan jika berubah
        //inisialisasi status tampilan awal jika belum inisialisasi
        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                if (mRealmResultsSetelan != null && mRealmResultsSetelan.isLoaded()) {

                    RMSetelan rmSetelan = mRealmResultsSetelan.first();
                    mRMSetelan = mRealm.copyFromRealm(rmSetelan);

                    //setel tanda awal pada tombol centangan
                    if (!sudahInisialisasi) {

                        inisialisasiTampilan();
                    }
                }
            }
        };


        mRealmQuerySetelan = mRealm.where(RMSetelan.class);
        mRealmResultsSetelan = mRealmQuerySetelan.findAllAsync();
        mRealmResultsSetelan.addChangeListener(mRealmChangeListener);

    }


    //PERBARUI TAMPILAN UNTUK INISIALISASI AWAL
    private void inisialisasiTampilan() {

        sudahInisialisasi = true;

        boolean statusCentang = mRMSetelan.isNotifikasiCuaca();
        mSwitchCompatNotifikasi.setChecked(statusCentang);
        mSwitchCompatNotifikasi.setOnCheckedChangeListener(mOnCheckedChangeListener);

        setTeksKeterangan(statusCentang);
    }


    private void setTeksKeterangan(boolean statsCentang) {

        if (statsCentang) {
            mTextViewKeteranganTeksNotifikasi.setText(R.string.setelan_notifikasi_keterangan_ok);
        } else {
            mTextViewKeteranganTeksNotifikasi.setText(R.string.setelan_notifikasi_keterangan_tidak);
        }
    }


}
