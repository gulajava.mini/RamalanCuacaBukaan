package gulajava.ramalancuaca.aktivitas;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;
import java.util.concurrent.Callable;

import bolts.CancellationTokenSource;
import bolts.Continuation;
import bolts.Task;
import butterknife.Bind;
import butterknife.ButterKnife;
import gulajava.ramalancuaca.R;
import gulajava.ramalancuaca.adapters.AdapterViewPager;
import gulajava.ramalancuaca.database.RMSetelan;
import gulajava.ramalancuaca.database.RealmKonfigurasi;
import gulajava.ramalancuaca.dialogs.DialogTentangApl;
import gulajava.ramalancuaca.fragments.FragmentCuacaJam;
import gulajava.ramalancuaca.fragments.FragmentCuacaPerHari;
import gulajava.ramalancuaca.internets.GoogleClientBuilder;
import gulajava.ramalancuaca.internets.StatusApp;
import gulajava.ramalancuaca.internets.sinkronisasi.RCuacaSyncAdapter;
import gulajava.ramalancuaca.modellogika.Parsers;
import gulajava.ramalancuaca.models.MsgHasilGeocoder;
import gulajava.ramalancuaca.utilans.CekGPSNet;
import gulajava.ramalancuaca.utilans.Konstan;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Gulajava Ministudio.
 */
public class AktHalamanUtama extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks {


    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    private ActionBar mActionBar;
    @Bind(R.id.pager)
    ViewPager mViewPager;
    @Bind(R.id.tablayout)
    TabLayout mTabLayout;

    @Bind(R.id.gambar_backdrop)
    ImageView mImageViewHeaders;
    @Bind(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @Bind(R.id.appbarlayout)
    AppBarLayout mAppBarLayout;


    private FragmentCuacaJam mFragmentCuacaJam;
    private FragmentCuacaPerHari mFragmentCuacaPerHari;


    private String mStringAlamatPengguna = "";
    private String mStringAlamatPenggunaPanjang = "";
    private String mStringLatitude = "";
    private String mStringLongitude = "";

    private CekGPSNet mCekGPSNet;
    private Parsers mParsers;
    private GoogleApiClient mGoogleApiClient = null;
    private Location mLocation;
    private LokasiListener mLokasiListenerGPS;

    private boolean isAktivitasJalan = false;
    private boolean isPermissionOK = false;
    private boolean isProsesSimpan = false;

    private Realm mRealm;
    private RealmResults<RMSetelan> mRealmResultsSetelan;
    private RMSetelan mRMSetelanKopian;
    private RealmChangeListener mRealmChangeListener;
    private CancellationTokenSource mCancellationTokenSource;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.akt_halamanutama_kolaps);
        ButterKnife.bind(AktHalamanUtama.this);

        StatusApp.getInstance().setStatusAplikasiJalan(true);

        AktHalamanUtama.this.setSupportActionBar(mToolbar);
        mActionBar = AktHalamanUtama.this.getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setTitle(R.string.app_name);
        mActionBar.setSubtitle(R.string.keterangan_namalokasi_kosong);

        mFragmentCuacaJam = new FragmentCuacaJam();
        mFragmentCuacaPerHari = new FragmentCuacaPerHari();

        mCekGPSNet = new CekGPSNet(AktHalamanUtama.this);
        mParsers = new Parsers(AktHalamanUtama.this);
        mLokasiListenerGPS = new LokasiListener();

        RealmConfiguration realmConfiguration = RealmKonfigurasi.getKonfigurasiRealm(AktHalamanUtama.this);
        mRealm = Realm.getInstance(realmConfiguration);

        mViewPager.setOffscreenPageLimit(2);

        inisialisasiTampilan();
    }


    @Override
    protected void onResume() {
        super.onResume();

        mCancellationTokenSource = new CancellationTokenSource();
        isAktivitasJalan = true;

        //ambil data nama kota dari realm dan inisialisasi realm
        ambilNamaKotaDatabase();

        //aktifkan gps pengguna
        //aktifkan fungsi gps
        cekPermisiLokasi();
    }

    @Override
    protected void onPause() {
        super.onPause();

        isAktivitasJalan = false;
        mRealmResultsSetelan.removeChangeListeners();

        mCancellationTokenSource.cancel();

        //sebelum keluar dari aplikasi, matikan location service nya.
        matikanGoogleApiClient();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        StatusApp.getInstance().setStatusAplikasiJalan(false);
        mRealm.close();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        AktHalamanUtama.this.getMenuInflater().inflate(R.menu.menu_utama, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int kode = item.getItemId();

        switch (kode) {

            case R.id.action_segarkandata:

                //hanya segarkan jika permission lokasi dibolehkan
                if (isPermissionOK) {

                    tampilSnackbarNotifikasiLokasi(R.string.toast_notifikasisegar);
                    sinkronDataInternet();

                } else {
                    tampilSnackbar(R.string.toast_gagalpermission);
                }

                return true;

            case R.id.action_peta_googlemaps:

                bukaPetaGoogleMaps();
                return true;

            case R.id.action_setelanapl:

                Intent intentSetelan = new Intent(AktHalamanUtama.this, AktSetelan.class);
                AktHalamanUtama.this.startActivity(intentSetelan);

                return true;

            case R.id.action_tentangapl:

                tampilDialogTentangApl();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //INISIALISASI TAMPILAN
    private void inisialisasiTampilan() {

        //glide
        Glide.with(AktHalamanUtama.this).load(R.drawable.backdrop_hujan).into(mImageViewHeaders);

        mCollapsingToolbarLayout.setTitle(AktHalamanUtama.this.getResources().getString(R.string.app_name));
        mCollapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(AktHalamanUtama.this, R.color.putih));
        mCollapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(AktHalamanUtama.this, R.color.transparan));

        mAppBarLayout.setExpanded(true);
        mAppBarLayout.addOnOffsetChangedListener(mOnOffsetChangedListener);


        //setel viewpager
        setelViewPager();

        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setTabTextColors(ContextCompat.getColorStateList(AktHalamanUtama.this, R.color.tab_teks_color_selector));
        mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

    }


    public void inisialisasiTampilanBackdrop(int kodegambar) {

        Glide.with(AktHalamanUtama.this)
                .load(kodegambar)
                .into(mImageViewHeaders);
    }


    AppBarLayout.OnOffsetChangedListener mOnOffsetChangedListener = new AppBarLayout.OnOffsetChangedListener() {
        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

            //jika app bar berubah jadi collapse atau expanded
            //
        }
    };


    //SETEL FRAGMENT DAN VIEWPAGER
    private void setelViewPager() {

        AdapterViewPager adapterViewPager = new AdapterViewPager(AktHalamanUtama.this.getSupportFragmentManager());
        adapterViewPager.addFragment(mFragmentCuacaJam, "CUACA HARI INI");
        adapterViewPager.addFragment(mFragmentCuacaPerHari, "CUACA HARI BESOK");

        mViewPager.setAdapter(adapterViewPager);
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
    }


    ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    TabLayout.OnTabSelectedListener mOnTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            mViewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };


    //AMBIL NAMA KOTA DARI REALM DB
    private void ambilNamaKotaDatabase() {

        inisialisasiRealmListener();

        RealmQuery<RMSetelan> setelanRealmQuery = mRealm.where(RMSetelan.class);
        mRealmResultsSetelan = setelanRealmQuery.findAllAsync();
        mRealmResultsSetelan.addChangeListener(mRealmChangeListener);
    }


    //LISTENER REALM JIKA DATA ADA YANG BERUBAH
    private void inisialisasiRealmListener() {

        mRealmChangeListener = new RealmChangeListener() {
            @Override
            public void onChange() {

                if (mRealmResultsSetelan != null && mRealmResultsSetelan.isLoaded()) {

                    RMSetelan rmSetelan = mRealmResultsSetelan.first();
                    String namaKota = rmSetelan.getNamaLokasi();

                    mStringLatitude = rmSetelan.getStrLatitude();
                    mStringLongitude = rmSetelan.getStrLongitude();

                    if (namaKota.length() > 1) {
                        mActionBar.setSubtitle(namaKota);
                    } else {
                        String namakotakosong = AktHalamanUtama.this.getResources().getString(R.string.keterangan_namalokasi_kosong);
                        mActionBar.setSubtitle(namakotakosong);
                    }
                }
            }
        };
    }


    //CEK PERMISI LOKASI UNTUK ANDROID M 6.0
    @AfterPermissionGranted(Konstan.ID_MINTALOKASI)
    private void cekPermisiLokasi() {

        if (EasyPermissions.hasPermissions(AktHalamanUtama.this, Konstan.PERMISSIONS)) {

            isPermissionOK = true;
            //sudah diijinkan minta lokasi
            //lanjut cek kondisi gps pengguna
            cekGpsPengguna();

        } else {

            isPermissionOK = false;
            EasyPermissions.requestPermissions(AktHalamanUtama.this, AktHalamanUtama.this.getResources().getString(R.string.keterangan_rasional),
                    Konstan.ID_MINTALOKASI, Konstan.PERMISSIONS);
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        isPermissionOK = true;
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        isPermissionOK = false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, AktHalamanUtama.this);
    }


    //CEK STATUS GPS
    //JALANKAN LISTENER LOKASI
    private void cekGpsPengguna() {

        boolean isInternet = mCekGPSNet.cekStatusInternet();
        boolean isNetworkNyala = mCekGPSNet.cekStatusNetworkGSM();
        boolean isGPSNetwork = mCekGPSNet.cekStatusNetwork();

        if (isInternet && isGPSNetwork || isNetworkNyala && isGPSNetwork) {

            if (mGoogleApiClient == null) {

                mGoogleApiClient = GoogleClientBuilder.getLocationClient(AktHalamanUtama.this,
                        mConnectionCallbacksOk, mOnConnectionFailedListener);
            }

            //sambungkan ke google play service location
            jalanGoogleApiClient();

        } else {

            tampilSnackbar(R.string.toast_gagalkoneksigps);
        }
    }


    private void tampilSnackbar(int kodepesan) {

        //beri notifikasi bahwa gps tidak aktif
        //sehingga gagal mengambil lokasi
        Snackbar.make(mToolbar, kodepesan, Snackbar.LENGTH_LONG)
                .setAction("SETEL", mOnClickListenerSnackbar)
                .setActionTextColor(ContextCompat.getColor(AktHalamanUtama.this, R.color.colorAccent))
                .show();
    }


    private void tampilSnackbarNotifikasiLokasi(int kodepesan) {
        //beri notifikasi bahwa gps tidak aktif
        //sehingga gagal mengambil lokasi
        Snackbar.make(mToolbar, kodepesan, Snackbar.LENGTH_SHORT)
                .setAction("OK", mOnClickListenerSnackbarKosong)
                .setActionTextColor(ContextCompat.getColor(AktHalamanUtama.this, R.color.colorAccent))
                .show();
    }

    //LISTENER LOKASI SETELAN
    View.OnClickListener mOnClickListenerSnackbar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            try {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                AktHalamanUtama.this.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    View.OnClickListener mOnClickListenerSnackbarKosong = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };


    //LISTENER LOKASI
    protected class LokasiListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (location != null && !isProsesSimpan) {

                //update data lokasi pengguna
                boolean adaInternet = mCekGPSNet.cekStatusInternet();
                if (adaInternet) {
                    isProsesSimpan = true;
                    //ambil lokasi baru dengan thread asynctask
                    mLocation = location;

                    mStringLatitude = mLocation.getLatitude() + "";
                    mStringLongitude = mLocation.getLongitude() + "";

                    jalanKanTaskGeocoder();

                } else {
                    isProsesSimpan = false;
                }
            }
        }
    }


    //CEK INTERNET
    //AMBIL GEOCODER
    //TASK ASYNCTASK
    private void jalanKanTaskGeocoder() {

        Task.callInBackground(new Callable<MsgHasilGeocoder>() {
            @Override
            public MsgHasilGeocoder call() throws Exception {

                return mParsers.ambilGeocoderPengguna(mStringLatitude, mStringLongitude);
            }
        }, mCancellationTokenSource.getToken())

                .continueWith(new Continuation<MsgHasilGeocoder, Object>() {
                    @Override
                    public Object then(Task<MsgHasilGeocoder> task) throws Exception {

                        if (isAktivitasJalan) {

                            MsgHasilGeocoder msgHasilGeocoder = task.getResult();

                            mStringAlamatPengguna = msgHasilGeocoder.getAlamatPendek();
                            mStringAlamatPenggunaPanjang = msgHasilGeocoder.getAlamatgabungan();

                            if (mStringAlamatPengguna.length() > 1) {

                                //simpan data
                                simpanDataSetelan();

                            } else {
                                isProsesSimpan = false;
                            }
                        } else {
                            isProsesSimpan = false;
                        }


                        return null;
                    }
                }, Task.UI_THREAD_EXECUTOR, mCancellationTokenSource.getToken());

    }


    //SIMPAN DATA KE DATABASE SETELAN
    private void simpanDataSetelan() {

        if (mRealmResultsSetelan != null && mRealmResultsSetelan.isLoaded()) {

            RMSetelan rmSetelan = mRealmResultsSetelan.first();
            mRMSetelanKopian = mRealm.copyFromRealm(rmSetelan);

            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    mRMSetelanKopian.setStrLatitude(mStringLatitude);
                    mRMSetelanKopian.setStrLongitude(mStringLongitude);
                    mRMSetelanKopian.setNamaLokasi(mStringAlamatPengguna);
                    mRMSetelanKopian.setNamaLokasiPanjang(mStringAlamatPenggunaPanjang);

                    realm.copyToRealmOrUpdate(mRMSetelanKopian);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {

                    isProsesSimpan = false;

                    //sukses simpan lokasi, perbarui cuaca
                    perbaruiCuaca();

                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {

                    error.printStackTrace();
                    isProsesSimpan = false;
                }
            });
        }
    }


    //SINKRON KAN DATA
    private void sinkronDataInternet() {

        RCuacaSyncAdapter.initializeSyncAdapter(AktHalamanUtama.this);
        RCuacaSyncAdapter.syncSegera(AktHalamanUtama.this);
    }


    //TAMPILKAN DIALOG
    private void tampilDialogTentangApl() {

        DialogTentangApl dialogTentangApl = new DialogTentangApl();
        FragmentTransaction fragmentTransaction = AktHalamanUtama.this.getSupportFragmentManager().beginTransaction();
        dialogTentangApl.show(fragmentTransaction, "tentang aplikasi");
    }


    //BUKA PETA GOOGLE MAPS
    private void bukaPetaGoogleMaps() {

        if (mStringLatitude.length() > 1 && mStringLongitude.length() > 1) {

            Uri geoLocation = Uri.parse("geo:" + mStringLatitude + "," + mStringLongitude);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(geoLocation);

            if (intent.resolveActivity(AktHalamanUtama.this.getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }


    //PERBARUI CUACA JIKA LOKASI BERUBAH
    private void perbaruiCuaca() {

        try {
            RCuacaSyncAdapter.syncSegera(AktHalamanUtama.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * ======  GOOGLE PLAY SERVICE LOCATION ==========
     **/


    //JALANKAN GOOGLE API CLIENT
    //MULAI API CLIENT
    public void jalanGoogleApiClient() {

        if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }


    //MATIKAN API CLIENT LOKASI
    public void matikanGoogleApiClient() {

        if (mLokasiListenerGPS != null && mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLokasiListenerGPS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    //LISTENER SAMBUNGAN OK KE GOOGLE PLAY SERVICE
    GoogleApiClient.ConnectionCallbacks mConnectionCallbacksOk = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(@Nullable Bundle bundle) {

            if (ActivityCompat.checkSelfPermission(AktHalamanUtama.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(AktHalamanUtama.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }

            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationRequest locationRequest = GoogleClientBuilder.getLocationGPSReq();
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, mLokasiListenerGPS);
        }

        @Override
        public void onConnectionSuspended(int i) {

            mGoogleApiClient.connect();

            //tampilkan proses sambung ulang
            tampilSnackbarNotifikasiLokasi(R.string.toast_prosesambilokasi);
        }
    };


    //LISTENER SAMBUNGAN GAGAL KE GOOGLE PLAY SERVICE
    GoogleApiClient.OnConnectionFailedListener mOnConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            //coba sambungan terus google api client
            if (!mGoogleApiClient.isConnecting() && !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }

            //tampilkan proses sambung ulang
            tampilSnackbarNotifikasiLokasi(R.string.toast_prosesambilokasi);
        }
    };

}
