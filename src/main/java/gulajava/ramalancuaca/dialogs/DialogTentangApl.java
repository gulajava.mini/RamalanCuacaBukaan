package gulajava.ramalancuaca.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import gulajava.ramalancuaca.R;

/**
 * Created by Gulajava Ministudio
 */
public class DialogTentangApl extends DialogFragment {


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        View view = LayoutInflater.from(DialogTentangApl.this.getActivity()).inflate(R.layout.tentang_apl, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(DialogTentangApl.this.getActivity());
        builder.setView(view);
        builder.setNegativeButton(R.string.tomboldialog_tutup, mOnClickListener);

        return builder.create();
    }


    DialogInterface.OnClickListener mOnClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {

            DialogTentangApl.this.getDialog().dismiss();

        }
    };
}
