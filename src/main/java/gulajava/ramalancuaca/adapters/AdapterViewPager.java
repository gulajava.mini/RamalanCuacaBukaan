package gulajava.ramalancuaca.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio.
 */
public class AdapterViewPager extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mStringList = new ArrayList<>();

    public AdapterViewPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        super.getPageTitle(position);

        return mStringList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        super.getItemPosition(object);
        return FragmentStatePagerAdapter.POSITION_NONE;
    }

    public void addFragment(Fragment fragment, String judulTab) {

        mFragmentList.add(fragment);
        mStringList.add(judulTab);
    }
}
